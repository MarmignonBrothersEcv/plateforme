import $ from 'jquery'
import MicroModal from 'micromodal'

export default class Modal {
  constructor(selector) {
    this.signBtn = '.carousel__session-sign'
    this.closeSignBtn = '.close-sign-btn'
    this.signModal = '.sign-modal'
  }

  init() {
    this.openSignModal()
  }

  // Open signature modal on click
  openSignModal() {
    let self = this
    $(self.signBtn).on('click', function() {
      $(self.signModal).addClass('is-active')
    })
    $(self.closeSignBtn).on('click', function() {
      $(self.signModal).removeClass('is-active')
    })
  }
}