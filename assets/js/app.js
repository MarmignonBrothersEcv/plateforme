/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

import '../css/config/reset.scss';
import '../css/config/variables.scss';
import '../css/config/mixins.scss';
import '../css/config/general.scss';

import '../css/utils/buttons.scss';
import '../css/utils/inputs.scss';
import '../css/utils/typography.scss';

import '../css/layout/header.scss';

import '../css/components/introduction-block.scss';
import '../css/components/medium-block.scss';
import '../css/components/dropdown-block.scss';
import '../css/components/small-block.scss';
import '../css/components/large-block.scss';

import '../css/pages/user-formations.scss';

// start the Stimulus application
/* import '../bootstrap'; */

// js files
/* import Formations from './user/Formations';
import Slider from './Slider';
import Modal from './Modal'; */

/* class App {
  constructor() {
    this.formations = new Formations()
    this.slider = new Slider()
    this.modal = new Modal()
  }

  init() {
    this.formations.init()
    this.slider.init()
    this.modal.init()
  }
}

document.addEventListener('DOMContentLoaded', function () {
  var app = new App()
  app.init()
})
 */