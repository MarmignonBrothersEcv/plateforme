import $ from 'jquery'

export default class Formations {
  constructor(selector) {
    this.formationBtn = '.header__item--formation'
    this.sessionsBtn = '.header__item--sessions'
    this.resultsBtn = '.header__item--results'
    this.formationDetails = '.formations-details'
    this.sessionDetails = '.session-details'
    this.resultDetails = '.result-details'
  }

  init() {
    this.switchInformations()
  }

  // Switch user's formation informations
  switchInformations() {
    let self = this
    $(self.formationBtn).addClass('is-active')
    $(self.formationDetails).addClass('is-active')
    $(self.formationBtn).on('click', function() {
      $(self.formationBtn).addClass('is-active')
      $(self.formationDetails).addClass('is-active')
      $(self.sessionsBtn).removeClass('is-active')
      $(self.sessionDetails).removeClass('is-active')
      $(self.resultsBtn).removeClass('is-active')
      $(self.resultDetails).removeClass('is-active')
    })
    $(self.sessionsBtn).on('click', function() {
      $(self.formationBtn).removeClass('is-active')
      $(self.formationDetails).removeClass('is-active')
      $(self.sessionsBtn).addClass('is-active')
      $(self.sessionDetails).addClass('is-active')
      $(self.resultsBtn).removeClass('is-active')
      $(self.resultDetails).removeClass('is-active')
    })
    $(self.resultsBtn).on('click', function() {
      $(self.formationBtn).removeClass('is-active')
      $(self.formationDetails).removeClass('is-active')
      $(self.sessionsBtn).removeClass('is-active')
      $(self.sessionDetails).removeClass('is-active')
      $(self.resultsBtn).addClass('is-active')
      $(self.resultDetails).addClass('is-active')
    })
  }
}