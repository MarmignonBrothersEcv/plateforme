import $ from 'jquery'
import 'slick-carousel'
import 'slick-carousel/slick/slick.scss'

export default class Slider {
  constructor(selector) {}

  init() {
    this.sessionsSlider()
  }

  // User's sessions slider

  sessionsSlider() {
    $('.session-details__carousel').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true
    })
  }
}