<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210515144724 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE session ADD morning_start_time TIME NOT NULL, ADD afternoon_end_time TIME NOT NULL, ADD afternoon_start_time TIME NOT NULL, ADD morning_end_time TIME NOT NULL, DROP start_time, DROP end_time, DROP moment');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE session ADD start_time TIME NOT NULL, ADD end_time TIME NOT NULL, ADD moment VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP morning_start_time, DROP afternoon_end_time, DROP afternoon_start_time, DROP morning_end_time');
    }
}
