<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     *@ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     *@ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthdate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $profil;

    /**
     * @ORM\OneToMany(targetEntity=Trainings::class, mappedBy="teacher")
     */
    private $trainings;

    /**
     * @ORM\ManyToMany(targetEntity=Trainings::class, mappedBy="participants")
     */
    private $participationTrainings;

    /**
     * @ORM\ManyToMany(targetEntity=Session::class, mappedBy="participants")
     */
    private $participationSession;

    public function __construct()
    {
        $this->trainings = new ArrayCollection();
        $this->participationTrainings = new ArrayCollection();
        $this->participationSession = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setphone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getProfil(): ?string
    {
        return $this->profil;
    }

    public function setProfil(string $profil): self
    {
        $this->profil = $profil;

        return $this;
    }

    /**
     * @return Collection|Trainings[]
     */
    public function getTrainings(): Collection
    {
        return $this->trainings;
    }

    public function addTraining(Trainings $training): self
    {
        if (!$this->trainings->contains($training)) {
            $this->trainings[] = $training;
            $training->setTeacher($this);
        }

        return $this;
    }

    public function removeTraining(Trainings $training): self
    {
        if ($this->trainings->removeElement($training)) {
            // set the owning side to null (unless already changed)
            if ($training->getTeacher() === $this) {
                $training->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Trainings[]
     */
    public function getParticipationTrainings(): Collection
    {
        return $this->participationTrainings;
    }

    public function addParticipationTraining(Trainings $participationTraining): self
    {
        if (!$this->participationTrainings->contains($participationTraining)) {
            $this->participationTrainings[] = $participationTraining;
            $participationTraining->addParticipant($this);
        }

        return $this;
    }

    public function removeParticipationTraining(Trainings $participationTraining): self
    {
        if ($this->participationTrainings->removeElement($participationTraining)) {
            $participationTraining->removeParticipant($this);
        }

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getParticipationSession(): Collection
    {
        return $this->participationSession;
    }

    public function addParticipationSession(Session $participationSession): self
    {
        if (!$this->participationSession->contains($participationSession)) {
            $this->participationSession[] = $participationSession;
            $participationSession->addParticipant($this);
        }

        return $this;
    }

    public function removeParticipationSession(Session $participationSession): self
    {
        if ($this->participationSession->removeElement($participationSession)) {
            $participationSession->removeParticipant($this);
        }

        return $this;
    }
}
