<?php

namespace App\Entity;

use App\Repository\SessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SessionRepository::class)
 */
class Session
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="time")
     */
    private $morningStartTime;

     /**
     * @ORM\Column(type="time")
     */
    private $morningEndTime;

     /**
     * @ORM\Column(type="time")
     */
    private $afternoonStartTime;

    /**
     * @ORM\Column(type="time")
     */
    private $afternoonEndTime;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    

    /**
     * @ORM\ManyToOne(targetEntity=Trainings::class, inversedBy="sessions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $training;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="participationSession")
     * @ORM\JoinTable(name="sessions_user")
     */
    private $participants;

    public function __construct()
    {
        $this->participants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getMorningStartTime(): ?\DateTimeInterface
    {
        return $this->morningStartTime;
    }

    public function setMorningStartTime(\DateTimeInterface $morningStartTime): self
    {
        $this->morningStartTime = $morningStartTime;

        return $this;
    }

    public function getMorningEndTime(): ?\DateTimeInterface
    {
        return $this->morningEndTime;
    }

    public function setMorningEndTime(\DateTimeInterface $morningEndTime): self
    {
        $this->morningEndTime = $morningEndTime;

        return $this;
    }

    public function getAfternoonStartTime(): ?\DateTimeInterface
    {
        return $this->afternoonStartTime;
    }

    public function setAfternoonStartTime(\DateTimeInterface $afternoonStartTime): self
    {
        $this->afternoonStartTime = $afternoonStartTime;

        return $this;
    }

    public function getAfternoonEndTime(): ?\DateTimeInterface
    {
        return $this->afternoonEndTime;
    }

    public function setAfternoonEndTime(\DateTimeInterface $afternoonEndTime): self
    {
        $this->afternoonEndTime = $afternoonEndTime;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTraining(): ?Trainings
    {
        return $this->training;
    }

    public function setTraining(?Trainings $training): self
    {
        $this->training = $training;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(User $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants[] = $participant;
        }

        return $this;
    }

    public function removeParticipant(User $participant): self
    {
        $this->participants->removeElement($participant);

        return $this;
    }
}
