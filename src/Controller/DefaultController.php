<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
class DefaultController extends AbstractController
{
        /**
         * @Route("/", name="homepage")
         */
        public function home()
        {
            $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
            return $this->render('default/index.html.twig');
        }
    }

?>