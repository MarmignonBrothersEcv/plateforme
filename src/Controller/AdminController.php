<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends AbstractController
{
    /**
     * @Route("/users", name="users_list")
     * 
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(): Response
    {
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
        return $this->render('user/list.html.twig', [
            'controller_name' => 'TrainingsController',
            'users' => $users
        ]);
    }

    /**
     * @Route("/user/{id}", name="user")
     * 
     * @IsGranted("ROLE_ADMIN")
     */
    public function userDetail(int $id): Response
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);

        return $this->render('user/index.html.twig', [
            'controller_name' => 'TrainingsController',
            'user' => $user
        ]);
    }

    /**
     * @Route("/edit_user/{id}", name="edit_user")
     * 
     * @IsGranted("ROLE_ADMIN")
     */
    public function editSession(Request $request, int $id): Response
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('user', ['id' => $user->getid()]);
        } 

        return $this->render('user/edit.html.twig', [
            'userForm' => $form->createView(),
            'user' => $user
        ]);
    }

    /**
     * @Route("/delete_user/{id}", name="delete_user")
     * 
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteUser( int $id)
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        return $this->redirectToRoute('users_list');
    }
}
