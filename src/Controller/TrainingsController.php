<?php

namespace App\Controller;

use App\Entity\Session;
use App\Entity\Trainings;
use App\Form\ParticipateTrainingFormType;
use App\Form\TrainingsFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class TrainingsController extends AbstractController
{
    /**
     * @Route("/trainings", name="trainings")
     * 
     */
    public function index(): Response
    {
        $trainings = $this->getDoctrine()
            ->getRepository(Trainings::class)
            ->findAll();
        return $this->render('trainings/index.html.twig', [
            'controller_name' => 'TrainingsController',
            'trainings' => $trainings
        ]);
    }

    /**
     * @Route("/training/{id}", name="training")
     * 
     */
    public function detail(int $id): Response
    {

        $training = $this->getDoctrine()
            ->getRepository(Trainings::class)
            ->find($id);
        $sessions = $this->getDoctrine()
        ->getRepository(Session::class)
        ->findBy(['training' => $id], ['date' => 'DESC']);

        return $this->render('trainings/detail.html.twig', [
            'controller_name' => 'TrainingsController',
            'training' => $training,
            'sessions' => $sessions
        ]);
    }

    /**
     * @Route("/add_training", name="add_training")
     * 
     * @IsGranted("ROLE_ADMIN")
     */
    public function addTraining(Request $request): Response
    {
        $training = new Trainings();
        $form = $this->createForm(TrainingsFormType::class, $training);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($training);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('trainings');
        } 

        return $this->render('trainings/add.html.twig', [
            'trainingForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit_training/{id}", name="edit_training")
     * 
     * @IsGranted("ROLE_ADMIN")
     */
    public function editTraining(Request $request, int $id): Response
    {
        $training = $this->getDoctrine()
            ->getRepository(Trainings::class)
            ->find($id);
        $form = $this->createForm(TrainingsFormType::class, $training);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($training);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('trainings');
        } 

        return $this->render('trainings/edit.html.twig', [
            'trainingForm' => $form->createView(),
            'training' => $training
        ]);
    }

    /**
     * @Route("/delete_training/{id}", name="delete_training")
     * 
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteSession( int $id)
    {
        $training = $this->getDoctrine()
            ->getRepository(Trainings::class)
            ->find($id);

        $sessions = $this->getDoctrine()
            ->getRepository(Session::class)
            ->findBy(['training' => $id]);

        $entityManager = $this->getDoctrine()->getManager();

        foreach( $sessions as $session){
            $entityManager->remove($session);
            $entityManager->flush();
        }
        
        $entityManager->remove($training);
        $entityManager->flush();

        return $this->redirectToRoute('trainings');
    }

    /**
     * @Route("/add_participant_training/{id}", name="add_participant_training")
     * 
     * @IsGranted("ROLE_ADMIN")
     */
    public function addParticipantToTraining(Request $request, int $id): Response
    {
        $training = $this->getDoctrine()
            ->getRepository(Trainings::class)
            ->find($id);
        $form = $this->createForm(ParticipateTrainingFormType::class, $training);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($training);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('trainings');
        } 

        return $this->render('trainings/register.html.twig', [
            'trainingForm' => $form->createView(),
            'training' => $training
        ]);
    }
    
}
