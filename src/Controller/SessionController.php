<?php

namespace App\Controller;

use App\Entity\Session;
use App\Entity\Trainings;
use App\Form\SessionFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class SessionController extends AbstractController
{
    /**
     * @Route("/add_session/{id}", name="add_session")
     * 
     * @IsGranted("ROLE_ADMIN")
     */
    public function addSessionToTraining(Request $request, int $id): Response
    {
        $session = new Session();
        $form = $this->createForm(SessionFormType::class, $session);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($session);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('training', ['id' => $id]);
        } 

        $training = $this->getDoctrine()
            ->getRepository(Trainings::class)
            ->find($id);

        return $this->render('session/add.html.twig', [
            'sessionForm' => $form->createView(),
            'training' => $training
        ]);
    }

    /**
     * @Route("/edit_session/{id}", name="edit_session")
     * 
     * @IsGranted("ROLE_ADMIN")
     */
    public function editSession(Request $request, int $id): Response
    {
        $session = $this->getDoctrine()
            ->getRepository(Session::class)
            ->find($id);
        $trainingId = $session->getTraining()->getId();
        $form = $this->createForm(SessionFormType::class, $session);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($session);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('training', ['id' => $trainingId]);
        } 

        return $this->render('session/edit.html.twig', [
            'sessionForm' => $form->createView(),
            'session' => $session
        ]);
    }

    /**
     * @Route("/delete_session/{id}", name="delete_session")
     * 
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteSession( int $id)
    {
        $session = $this->getDoctrine()
            ->getRepository(Session::class)
            ->find($id);
        $trainingId = $session->getTraining()->getId();
        
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($session);
        $entityManager->flush();

        return $this->redirectToRoute('training', ['id' => $trainingId]);
    }
}
