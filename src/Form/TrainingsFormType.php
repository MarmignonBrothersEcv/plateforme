<?php

namespace App\Form;

use App\Entity\Trainings;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class TrainingsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
            ])
            ->add('date', DateType::class, [
                'required' => true,
            ])
            ->add('duration',  TimeType::class, [
                'required' => false,
                'label' => 'Durée ( en h )'
            ])
            ->add('location', TextType::class, [
                'required' => true,
            ])
            ->add('teacher', EntityType::class, [
                // looks for choices from this entity
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.roles LIKE :role')
                        ->setParameter('role', '%"'.'ROLE_TEACHER'.'"%');
                },
                // uses the User.username property as the visible option string
                'choice_label' => 'firstname ',
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Trainings::class,
        ]);
    }
}
