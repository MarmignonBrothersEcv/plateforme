<?php

namespace App\Form;

use App\Entity\Session;
use App\Entity\Trainings;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class SessionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
            ])
            ->add('date', DateType::class, [
                'required' => false,
            ])
            ->add('morningStartTime',  TimeType::class, [
                'required' => false,
                'label' => 'Début'
            ])
            ->add('morningEndTime',  TimeType::class, [
                'required' => false,
                'label' => 'Fin'
            ])
            ->add('afternoonStartTime',  TimeType::class, [
                'required' => false,
                'label' => 'Début'
            ])
            ->add('afternoonEndTime',  TimeType::class, [
                'required' => false,
                'label' => 'Fin'
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
            ])
            ->add('training', EntityType::class, [
                // looks for choices from this entity
                'class' => Trainings::class,
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Session::class,
        ]);
    }
}
